defmodule Devafrica.Article do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Devafrica.Repo

  @moduledoc """
  Devafrica keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @doc """
  Parse article content and save
  """

  schema "articles" do
    field :title, :string
    field :content, :string
  end

  def changeset(struct \\ %__MODULE__{}, params) do
    cast(struct, params, [:title, :content])
    |> validate_required([:title, :content])
  end

  def save(%{"article_id" => id} = params) do
    Repo.get(__MODULE__, id)
    |> changeset(params)
    |> Repo.update
  end

  def delete(id) do
    Repo.get(Article, id)
    |> Repo.delete()
  end

  def get_article(title) do
    from(a in __MODULE__, where: a.title == ^title)
    |> Repo.one()
  end

  def get_last_saved() do
    from(a in __MODULE__, order_by: :id)
    |> Repo.all()
    |> List.last
  end
end
