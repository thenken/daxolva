defmodule DevafricaWeb.PageController do
  use DevafricaWeb, :controller
  alias Devafrica.Article

  @placeholder """
    <h1>Starting writing here
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </h1>
  """

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def about(conn, _params) do
    render(conn, "about.html")
  end

  def contact(conn, _params) do
    render(conn, "contact.html")
  end

  def jobs(conn, _params) do
    render(conn, "jobs.html")
  end

  def blog(conn, _params) do
    render(conn, "blog.html")
  end

  def read(conn, _params) do
    render(conn, "blog-read.html")
  end

  def write(conn, _params) do
    {id, title, content} = 
      case Article.get_last_saved() || Article.get_article("placeholder") do
        nil -> { nil, "Placeholder Title", @placeholder }
        %{id: id, title: title, content: content} -> { id, title, content }
      end

    render(conn, "writer.html", content: content, title: title, article_id: id)
  end

  def dashboard(conn, _params) do
    render(conn, "dashboard.html")
  end

  def save_article(conn, %{"content" => content, "title" => title, "article_id" => id} = params) do
    case Article.save(params) do
      {:ok, _article} -> put_flash(conn, :info, "Article saved")
      {:error, _changeset} -> put_flash(conn, :error, "Article NOT saved")
    end

    render(conn, "writer.html", content: content, title: title, article_id: id)
  end
end
