defmodule DevafricaWeb.PageView do
  use DevafricaWeb, :view
  alias Devafrica.Article

  def renderer() do
    %{content: content} = Article.get_last_saved()
  
    Phoenix.HTML.raw(content)
  end
end
