defmodule Devafrica.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Devafrica.Repo,
      # Start the Telemetry supervisor
      DevafricaWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Devafrica.PubSub},
      # Start the Endpoint (http/https)
      DevafricaWeb.Endpoint
      # Start a worker by calling: Devafrica.Worker.start_link(arg)
      # {Devafrica.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Devafrica.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DevafricaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
