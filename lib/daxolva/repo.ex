defmodule Devafrica.Repo do
  use Ecto.Repo,
    otp_app: :Devafrica,
    adapter: Ecto.Adapters.Postgres
end
