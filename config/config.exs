# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :Devafrica,
  ecto_repos: [Devafrica.Repo]

# Configures the endpoint
config :Devafrica, DevafricaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aHrR0r+GCLhEFMpVDycMTRzsRro5t/XX8RCmH2VMvXjkC43IBRwmPXUwuRoD5Nwg",
  render_errors: [view: DevafricaWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Devafrica.PubSub,
  live_view: [signing_salt: "AiDUaTHu"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
