import "../css/app.scss"
import "phoenix_html";
import "./blog.js"
import "./d.js"
import "bootstrap"

import Axios from 'axios'
Axios.defaults.headers.common["X-CSRF-TOKEN"] = window.CSRF_TOKEN;

import Vue from 'vue'
window.Vue = Vue

import Info from "@/vue/info"
import Writer from "@/vue/writer"

export default{
    Info,
    Writer
}
