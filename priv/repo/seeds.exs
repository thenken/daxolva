# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:

Devafrica.Repo.insert!(%Devafrica.Article{ title: "placeholder", content:
    """
        <h1>Starting writing here
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </h1>
    """
})

# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
