defmodule Devafrica.Repo.Migrations.CreateArticlesTable do
  use Ecto.Migration

  def change do
    create table "articles" do
      add :title, :string
      add :content, :text
    end
  end
end
